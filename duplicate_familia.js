'use strict';
// Para las consultas en la base de datos la libreria bluebird
let promise = require ('bluebird');
const colors = require('colors')
// Para la lectura de las variables de entorno dotenv
require('dotenv').load();


let options = {
    // Initialization Options
    promiseLib: promise,
    noWarnings: true,
    // Para mostrar el log de las querys
    query(e) {
        console.log(e.query);
    }
};
// Para el manejo de libreria con postgresql
let pgp = require('pg-promise')(options);
let connectionString = 'postgres://'+process.env.USUARIO+':'+process.env.CLAVE+'@'+process.env.HOSTDIRECCION+':'+process.env.PUERTO+'/'+process.env.BD;
let db = pgp(connectionString);

db.tx(t => {
    return t.batch([t.manyOrNone('SELECT * FROM familia'), t.any('SELECT * FROM local')]);
}).then(resp =>  {
    //let familias = resp[0];
    //let locales = resp[1];
    db.tx(t => {
        t.none('TRUNCATE familia')
        return resp;

    }).then( resp =>{

        resp[1].forEach( (local) =>{
            loopFamilia(resp[0], local.id_local, db);
        })
    });



}).catch(e =>{
    console.log(colors.red.bold(e.message));
    process.exit();
})


const guardarFamilia = (familia, idlocal, db) => new Promise(r =>
    db.tx(t => {
        return t.oneOrNone('SELECT * FROM familia_grupo where nombre like ${fam_descripcion}', familia).then(grupo => {
            if (grupo != null)
            {
                console.log("grupo: " + grupo)
                return grupo;
            }
            else
            {
                console.log("else: ")
                return t.one('INSERT INTO familia_grupo(\n' +
                    '            nombre, descripcion, estado \n' +
                    ') VALUES(${fam_descripcion}, ${fam_descripcion}, 1) RETURNING id_familia_grupo', familia);

            }

        })

    }).then(resp =>  {
        db.tx(t => {
            console.log("INSERTA")
            r(t.none('INSERT INTO familia(\n' +
                '            id_familiabase, id_familiareq, fam_nombre, fam_descripcion, \n' +
                '            fam_fmodificacion, fam_clase, fam_id_grupomayor, fam_max_descuento, fam_exenta, fam_maxreq, ecommerce, fam_color, fam_fondo, mensaje, prioridad, accesorio, tablet, activo, id_familia_grupo, id_familia_local, id_local' +
                ') VALUES(${id_familiabase}, ${id_familiareq}, ${fam_nombre}, ${fam_descripcion},${fam_fmodificacion}, ${fam_clase}, ${fam_id_grupomayor}, ${fam_max_descuento}, ${fam_exenta},' +
                ' ${fam_maxreq}, ${ecommerce}, ${fam_color}, ${fam_fondo}, ${mensaje}, ${prioridad}, ${accesorio}, ${tablet}, ${activo}, ' + resp.id_familia_grupo + ', ${id_familia_local}, '+ idlocal +')', familia));
        })
    })

)
    //return new Promise.resolve();


const loopFamilia = async (familiaList, idlocal,db) => {
    await asyncForEach(familiaList, async (familia) => {
        //await waitFor(50);
        await guardarFamilia(familia, idlocal, db)
    })

}


async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}



/*let generateInsert = (idlocal, familia) => {
   // let queryInsert =
    return `INSERT INTO public.familia(
            id_familiabase, id_familiareq, fam_nombre, fam_descripcion,
            fam_fmodificacion, fam_clase, fam_id_grupomayor, fam_max_descuento,
            fam_exenta, fam_maxreq, ecommerce, fam_color, fam_fondo, mensaje,
            prioridad, accesorio, tablet, id_familia_grupo, activo, id_familia_local,
            id_local, estado, created_at, updated_at)
            VALUES (${familia.id_familiabase}, ${familia.id_familiareq}, '${familia.fam_nombre}', '${familia.fam_descripcion}',
            now(), ${familia.fam_clase}, ${familia.fam_id_grupomayor}, ${familia.fam_max_descuento},
            ${familia.fam_exenta}, ${familia.fam_maxreq}, ${familia.ecommerce}, ${familia.fam_color}, ${familia.fam_fondo}, ${familia.mensaje},
            ${familia.prioridad}, ${familia.accesorio}, ${familia.tablet}, ${familia.id_familia_grupo}, ${familia.activo}, ${familia.id_familia_local},
            ${idlocal}, ${familia.estado}, now(),now());`;

};*/
